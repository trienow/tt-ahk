﻿#NoEnv ; Recommended for performance and compatibility with future AutoHotkey releases.
#Warn ; Enable warnings to assist with detecting common errors.
#SingleInstance Force
SendMode Input ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir% ; Ensures a consistent starting directory.

global fps := 7.50
global msPerFloor := 1000 / fps
global msPerFloor *= 0.93

global MsgBox1Btn := {x1: 881, y1: 631, x2: 1036, y2: 690, btn1x: 961, btn1y: 666}
global MsgBox2Btn := {x1: 785, y1: 329, x2: 1134, y2: 692, btn1x: 865, btn1y: 661, btn2x: 1050, btn2y: 661}
global IconBlockPos := {x1: 656, y1: 962, x2: 723, y2: 1030, btn1x: 661, btn1y: 996}
global FloorReq := {x1: 730, y1: 560, x2: 800, y2: 610 }
global ElevatorBtns := {x1: 824, y1: 838, x2: 1082, y2: 955, btn1x: 882, btn1y: 898, btn2x: 1023, btn2y: 898}
global CloseBtn := {x1: 1185, y1: 998, x2: 1260, y2: 1073, btn1x: 1223, btn1y: 1052}
global RaffleBtn := {btn1x: 1161, btn1y: 795, btn2x: 995, btn2y: 665}
global RestockFloor := {x1: 1130, y1: 130, x2: 1202, y2: 1010, btn0x: 0, btn0y: 0, btn1x: 1000, btn1y: 641, btn2x: 1000, btn2y: 517, btn3x: 1000, btn3y: 407}
global FloorStockStatus := {x1: 808, y1: 130, x2: 880, y2: 950, btn0x: 0, btn0y: 0, trans: "TransWhite"}
global StockAllFloors := {x1: 759, y1: 897, x2: 946, y2: 944, btn1x: 858, btn1y: 920}
global ScrollBottom := {btn1x: 959, btn1y: 1053}

global FreeStuff := {x1: 657, y1: 145, x2: 727, y2: 217, btn0x: 0, btn0y: 0}
global OpenFreeGift := {x1: 795, y1: 242, x2: 1119, y2: 316, btn0x: 0, btn0y: 0}

global PullDown := {x1: 1100, y1: 1, x2: 1100, y2: 550, btn1x: 1097, btn1y: 55} ;Cog button
global BackApp := {btn1x: 1910, btn1y: 555, btn2x: 1899, btn2y: 982} ;1.Sidebar 2.Button

global MissionBtn := {x1: 657, y1: 42, x2: 735, y2: 142, btn1x: 658, btn1y: 83}
global MissionDialog := {x1: 873, y1: 347, x2: 1042, y2: 387, btnSkipx: 859, btnSkipy: 761, btnContinuex: 1064, btnContinuey: 761}

global VipList := {x1: 822, y1: 369, x2: 1136, y2: 715, Δx: 281, Δy: 22, nextVipy: 72}

global AdvertButtons := {x1: 968, y1: 369, x2: 1122, y2: 752}

global ParachuteZone := {x1: 870, y1: 100, x2: 1060, y2: 1020, trans: "TransBlack"}

; EVICTER
global Unhappy := {x1: 1223, y1: 132, x2: 1258, y2: 959, itemHeight: 72, relStatsX: -300, relItemTop: -18} ; A Zone where unhappy.png can appear. relStatsX: Where relative to the frowny the stats begin
global DreamJobColors := []
DreamJobColors.Push({cDull: 0xC8CC94, cVibrant: 0xF6FAA5, img: "9_beige" })
DreamJobColors.Push({cDull: 0x4084B1, cVibrant: 0x42C8FF, img: "9_blue" })
DreamJobColors.Push({cDull: 0x529E4C, cVibrant: 0x67FF37, img: "9_green" })
DreamJobColors.Push({cDull: 0x9158B1, cVibrant: 0xE573FF, img: "9_magenta" })
DreamJobColors.Push({cDull: 0x9C5952, cVibrant: 0xFB7542, img: "9_red" })
global EvictBtn := {btn1x: 1077, btn1y: 723}

; ==========
global lastStockCheck := 19970225040000
global lastFreeStuffCheck := 19970225040000
global lastReload := A_Now

global lastFirstRentCollect := -1 ;Otherwise this is A_DD

global screenshotsTaken := 0

^F2::
    Pause, Toggle
Return

^F3::
    ; Debug
    VipHandler()
Return

^F4::
    Evicter()
Return

#IfWinActive ahk_exe Nox.exe
^F1::
    Gosub, RunLoop
Return
#IfWinActive

RunLoop:
    firstRun := True
    Loop
    {
        if(firstRun)
        {
            firstRun := False
        }
        else
        {
            Sleep, 5000
        }

        if(FindImage(MsgBox1Btn, 20, "continue"))
        {
            ClickBtn(MsgBox1Btn, 1)
            LogLastAction("continue")
            continue
        }

        if(FindImage(AdvertButtons, 20, "ad_loading"))
        {
            LogLastAction("ad_loading")
            continue
        }

        if(FindImage(AdvertButtons, 20, "ad_collect"))
        {
            ClickBtn(AdvertButtons, 0)
            Sleep, 1000
            MouseMove, 1210, 59, 90
            LogLastAction("ad_collect")
            Continue
        }

        if(FindImage(CloseBtn, 20, "closeBtn"))
        {
            ClickBtn(CloseBtn, 1)
            LogLastAction("closeBtn")
            continue
        }

        if(FindImage(MsgBox2Btn, 20, "nothanks_youbet"))
        {
            ClickBtn(MsgBox2Btn, 1)
            LogLastAction("nothanks_youbet")
            continue
        }

        if(FindImage(MsgBox2Btn, 20, "nothanks_continue"))
        {
            ClickBtn(MsgBox2Btn, 1)
            LogLastAction("nothanks_continue")
            Continue
        }

        if(FindImage(MsgBox2Btn, 20, "cancel_hurry"))
        {
            ClickBtn(MsgBox2Btn, 1)
            LogLastAction("cancel_hurry")
            Continue
        }

        if(FindImage(MsgBox2Btn, 20, "visit_continue"))
        {
            ClickBtn(MsgBox2Btn, 1)
            Sleep, 2000

            if(FindImage(MsgBox2Btn, 20, "details_continue"))
            {
                ClickBtn(MsgBox2Btn, 2)
                Sleep, 2000
            }

            while(!FindImage(MsgBox1Btn, 20, "continue"))
            {
                Sleep, 2000
            }
            ClickBtn(MsgBox1Btn, 1)

            LogLastAction("visit_continue")
            continue
        }

        while(FindImage(MsgBox2Btn, 20, "share_continue"))
        {
            ClickBtn(MsgBox2Btn, 2)
            Sleep, 1000
        }

        if(FindImage(MsgBox2Btn, 20, "details_continue"))
        {
            ClickBtn(MsgBox2Btn, 2)
            LogLastAction("details_continue")
            continue
        }

        if(RestockHandler())
        {
            LogLastAction("RestockHandler")
            Continue
        }

        if(EnterRaffle())
        {
            LogLastAction("EnterRaffle")
            Continue
        }

        if(FreeStuffHandler())
        {
            LogLastAction("FreeStuffHandler")
            Continue
        }

        if(FindImage(MissionBtn, 20, "missionAvailable"))
        {
            ClickBtn(MissionBtn, 1)
            Sleep, 1000
            if(FindImage(MissionDialog, 20, "deliver"))
            {
                ClickBtn(MissionDialog, "Continue")
            }
            else
            {
                ClickBtn(MissionDialog, "Skip")
                Sleep, 500
                ClickBtn(MsgBox2Btn, 2) ;Confirm Skip
            }

            LogLastAction("missionAvailable")
            Continue
        }

        if(FindImage(MissionBtn, 20, "missionComplete_0") || FindImage(MissionBtn, 20, "missionComplete_1"))
        {
            ClickBtn(MissionBtn, 1)
            Sleep, 1000
            ClickBtn(MissionDialog, "Continue")
            LogLastAction("missionComplete")
            Continue
        }

        if(FindImage(IconBlockPos, 50, "elevator"))
        {
            ClickBtn(IconBlockPos, 1)
            Sleep, 2000

            digits := []
            vari := 10
            floorReqTmp := FloorReq.Clone()

            Loop, 10
            {
                floorDigit := A_Index - 1
                digitFile := "floorreq\" floorDigit
                floorReqTmp.x1 := FloorReq.x1

                While, FindImage(floorReqTmp, vari, digitFile)
                {
                    digits[floorReqTmp.btn0x] := floorDigit
                    floorReqTmp.x1 := floorReqTmp.btn0x + 9 ;9 is the smallest width of a character
                }
            }

            digitCnt := digits.Count()
            digitIndex := 1
            floor := 0
            For k,v in digits
            {
                floorDigit := v
                Loop % (digitCnt - digitIndex)
                {
                    floorDigit *= 10
                }
                floor += floorDigit
                digitIndex++
            }

            floorLog := FileOpen("floor.log", "w")
            floorLog.Write(floor "`r`n")
            floorLog.Close()

            HoldBtn(ElevatorBtns, 1, msPerFloor * (floor - 1))

            LogLastAction("elevator")
            Continue
        }

        if(FindImage(IconBlockPos, 20, "rent"))
        {
            ClickBtn(IconBlockPos, 1)
            Sleep, 1000

            if(lastFirstRentCollect != A_DD)
            {
                ReloadAppHandler(True)
                lastFirstRentCollect := A_DD
            }

            if(FindImage(MsgBox1Btn, 20, "collect"))
            {
                ClickBtn(MsgBox1Btn, 1)
            }
            else
            {
                MsgBox, "Could not find collect after rent btn.
            }

            LogLastAction("rent")
            continue
        }

        if(FindImage(IconBlockPos, 20, "fireworks"))
        {
            ClickBtn(IconBlockPos, 1)
            Sleep, 1000

            LogLastAction("fireworks")
            continue
        }

        if(FindImage(IconBlockPos, 20, "vip"))
        {
            ClickBtn(IconBlockPos, 1)
            Sleep, 1000

            VipHandler()

            ClickBtn(CloseBtn, 1)

            LogLastAction("vip")
            continue
        }

        if(FindImage(IconBlockPos, 20, "Restock") || FindImage(IconBlockPos, 20, "RestockSlim"))
        {
            ClickBtn(IconBlockPos, 1)
            Sleep, 2000

            LogLastAction("Restock")
            Continue
        }

        if(StockAllHandler())
        {
            LogLastAction("StockAllHandler")
            Continue
        }

        if(ReloadAppHandler(False))
        {
            LogLastAction("ReloadAppHandler(False)")
            Continue
        }

        if(FindImage(ParachuteZone, 20, "parachute"))
        {
            ParachuteZone.btn0x += 75
            ParachuteZone.btn0y += 50
            ClickBtn(ParachuteZone, 0)
            LogLastAction("parachute")
            Continue
        }

        LogLastAction("none")
    }

^ESC::
    Reload
Return

LogLastAction(lastAction)
{
    /*
    actionLog := FileOpen("action.log", "rw")
    actionLog.Seek(0, 2)
    actionLog.Write(lastAction "`r`n")
    actionLog.Close()
    */
}

Evicter()
{
    foundUnhappy := false

    loop {
        foundUnhappy := false
        unhappyTmp := Unhappy.Clone()
        While(FindImage(unhappyTmp, 10, "unhappy"))
        {
            foundUnhappy := true
            halfHeight := (unhappyTmp.itemHeight / 2)
            stats := {}
            stats.x1 := unhappyTmp.btn0x + unhappyTmp.relStatsX
            stats.x2 := unhappyTmp.btn0x - 1
            stats.y1 := unhappyTmp.btn0y + unhappyTmp.relItemTop + halfHeight
            stats.y2 := stats.y1 + halfHeight
            stats.trans := "TransWhite"
            evicted := False

            for _key2, dreamColor in DreamJobColors
            {
                ; If I don't have a bitizen's dream job floor, it will be in the color of cDull
                ; A bitizen's stats will always be in the more vibrant cVibrant
                ; The dream job floor will always be located at the bottom half of the item, hence the halfHeight variable
                if(FindColor(stats, 11, dreamColor.cDull) || FindColor(stats, 11, dreamColor.cVibrant))
                {
                    stats.y1 -= halfHeight
                    stats.y2 -= halfHeight

                    ; Now lets see if the bitizen has a 9 stat in his dream job type
                    if(!FindImage(stats, 11, dreamColor.img))
                    {
                        ClickBtn(unhappyTmp, 0)
                        LogLastAction(dreamColor.img)

                        Sleep, 500
                        ClickBtn(EvictBtn, 1)
                        Sleep, 200
                        ClickBtn(MsgBox2Btn, 2)
                        Sleep, 800
                        evicted := true

                        if(!FindImage(CloseBtn, 20, "closeBtn"))
                        {
                            Sleep, 500
                        }
                    }
                    Break
                }
            }

            unhappyTmp.y1 := unhappyTmp.btn0y

            if(!evicted)
            {
                unhappyTmp.y1 += unhappyTmp.itemHeight
                if(unhappyTmp.y1 >= unhappyTmp.y2)
                {
                    Break
                }
            }
        }

        SendMode Event
        MouseMove, % Unhappy.x2, % Unhappy.y2, 1
        Send, "{LButton down}"
        MouseMove, % Unhappy.x1, % Unhappy.y1 + Unhappy.itemHeight, 50
        MouseMove, 0, 1, 10, R
        Send, "{LButton up}"
        MouseMove, -400, 0, 10, R
        Sleep, 1000
        SendMode Input
    }
    until !foundUnhappy
}

VipHandler()
{
    VipHandlerType("realEstateAgent", 0)
    VipHandlerType("construction", 0)
    vipCount := VipHandlerType("celebrity", 1)
    deliveryCount := VipHandlerType("delivery", 2)

    ; Make sure we have enough space for a delivery guy
    if(deliveryCount == 0)
    {
        VipHandlerType("bigSpender", 4 - vipCount)
    }
}

VipHandlerType(vipType, maxCount)
{
    img := "vip_" vipType
    vipCount := 0
    coords := VipList.Clone()
    while(coords.y1 < coords.y2 && FindImage(coords, 20, img))
    {
        vipCount += 1
        coords.y1 := coords.btn0y + VipList.nextVipy
        if(vipCount > maxCount)
        {
            coords.btn0x += VipList.Δx
            coords.btn0y += VipList.Δy
            ClickBtn(coords, 0)
            Sleep, 500
            ClickBtn(MsgBox2Btn, 2)
            Sleep, 1000
        }
    }

return vipCount
}

ReloadAppHandler(force)
{
    diff := A_Now
    diff -= lastReload, Minutes
    result := false
    if(force || (diff > 30 && FindImage(CloseBtn, 20, "menuBtn")))
    {
        SendMode Event
        MouseMove, % PullDown.x1, % PullDown.y1, 1
        Send, "{LButton down}"
        MouseMove, % PullDown.x2, % PullDown.y2, 19
        Send, "{LButton up}"
        Sleep, 200

        MouseMove, % PullDown.x1, % PullDown.y1, 1
        Send, "{LButton down}"
        MouseMove, % PullDown.x2, % PullDown.y2, 19
        Send, "{LButton up}"
        Sleep, 300
        SendMode Input

        ClickBtn(PullDown, 1)
        Sleep, 20000

        MouseMove, % BackApp.btn1x, % BackApp.btn1y, 1
        Sleep, 400
        ClickBtn(BackApp, 1)
        Sleep, 1000
        ClickBtn(BackApp, 2)
        Sleep, 1100

        if(FindImage(MsgBox1Btn, 20, "continue"))
        {
            ClickBtn(MsgBox1Btn, 1)
        }

        result := True
        lastReload := A_Now
    }
Return result
}

FreeStuffHandler()
{
    diff := A_Now
    diff -= lastFreeStuffCheck, Minutes
    result := false
    if(diff >= 5)
    {
        if(FindImage(FreeStuff, 20, "freeStuff"))
        {
            ClickBtn(FreeStuff, 0)
            Sleep, 1000
            if(FindImage(OpenFreeGift, 20, "openFreeGift"))
            {
                ClickBtn(OpenFreeGift, 0)
                Sleep, 1000

                if(FindImage(MsgBox1Btn, 20, "continue"))
                {
                    ClickBtn(MsgBox1Btn, 1)
                    Sleep, 1000
                }
                result := True
            }

            if(FindImage(CloseBtn, 20, "closeBtn"))
            {
                ClickBtn(CloseBtn, 1)
                Sleep, 1000
            }
        }

        lastFreeStuffCheck := A_Now
    }

Return result
}

StockAllHandler()
{
    diff := A_Now
    diff -= lastStockCheck, Minutes
    result := false

    if(diff > 15 && FindImage(CloseBtn, 20, "menuBtn"))
    {
        ClickBtn(ScrollBottom, 1)
        Sleep, 1500
        if(FindImage(StockAllFloors, 20, "stockAll"))
        {
            ClickBtn(StockAllFloors, 1)
            Sleep, 1000
            ClickBtn(MsgBox2Btn, 2) ;cancel_yes
        }
        lastStockCheck := A_Now
        result := true
    }

return result
}

RestockHandler()
{
    found := False
    While(FindImage(RestockFloor, 20, "RestockFloor"))
    {
        RestockFloor.btn0x -= 100
        StockProduct(RestockFloor)

        found := True
    }

    stats := [ "000", "001", "010", "011", "100", "101", "110" ]
    For k1, v1 in stats
    {
        While(FindImage(FloorStockStatus, 20, "StockStatus" v1))
        {
            FloorStockStatus.btn0x += 5
            FloorStockStatus.btn0y += 5
            StockProduct(FloorStockStatus)
            Sleep, 200

            found := True
        }
    }

Return found
}

StockProduct(CoordStruct)
{
    ClickBtn(CoordStruct, 0)
    Sleep, 1000

    if(FindImage(CloseBtn, 20, "closeBtn"))
    {
        ClickBtn(RestockFloor, 1)
        Sleep, 500
    }

    if(FindImage(CloseBtn, 20, "closeBtn"))
    {
        ClickBtn(RestockFloor, 2)
        Sleep, 500
    }

    if(FindImage(CloseBtn, 20, "closeBtn"))
    {
        ClickBtn(RestockFloor, 3)
        Sleep, 500
    }

    if(FindImage(CloseBtn, 20, "closeBtn"))
    {
        ClickBtn(CloseBtn, 1)
    }
}

EnterRaffle()
{
    enterRaffle := False

    if(FindImage(CloseBtn, 20, "menuBtn"))
    {
        file := "lastRaffle.ini"
        enterRaffle := !FileExist(file)

        If(!enterRaffle)
        {
            IniRead, lastRaffleTime, %file%, "Raffle", "lastRaffle", 19970101000000
            diff := A_Now
            diff -= lastRaffleTime, Minutes
            enterRaffle := diff > 59
        }

        if(enterRaffle)
        {
            ClickBtn(CloseBtn, 1)
            Sleep, 500
            ClickBtn(RaffleBtn, 1)
            Sleep, 10000
            ClickBtn(RaffleBtn, 2)
            Sleep, 5000
            ClickBtn(RaffleBtn, 2)
            Sleep, 500
            ClickBtn(CloseBtn, 1)
            Sleep, 500
            ClickBtn(CloseBtn, 1)
            Sleep, 500

            IniWrite, %A_Now%, %file%, "Raffle", "lastRaffle"
        }
    }

Return enterRaffle
}

ZeroPad(str, mask)
{
Return (SubStr(mask, 1, StrLen(mask) - StrLen(str)) . str)
}

ImgExists(name)
{
    file := A_ScriptDir "\img\" name ".png"
Return FileExist(file)
}

HoldBtn(coords, btnNo, length)
{
    MouseMove, % coords["btn" btnNo "x"], % coords["btn" btnNo "y"]
    Sleep, 10
    Send, "{LButton down}"
    Sleep, %length%
    Send, "{LButton up}"
}

ClickBtn(coords, btnNo)
{
    Click, % coords["btn" btnNo "x"] " " coords["btn" btnNo "y"]
}

FindImage(ByRef coords, variance, name)
{
    result := False
    img := A_ScriptDir "\img\" name ".png"
    trans := coords.trans

    if(trans)
    {
        ImageSearch, foundX, foundY, % coords.x1, % coords.y1, % coords.x2, % coords.y2, *%variance% *%trans% %img%
    }
    else
    {
        ImageSearch, foundX, foundY, % coords.x1, % coords.y1, % coords.x2, % coords.y2, *%variance% %img%
    }

    if(ErrorLevel = 2)
    {
        MsgBox, % "Failed to find image '" img "' at (" coords.x1 "," coords.y1 ") => (" coords.x2 "," coords.y2 ")"
    }
    else if(ErrorLevel = 0)
    {
        coords.btn0x := foundX
        coords.btn0y := foundY
        result := True
    }

return result
}

FindColor(ByRef coords, variance, rgbColor)
{
    result := False

    PixelSearch, foundX, foundY, % coords.x1, % coords.y1, % coords.x2, % coords.y2, % rgbColor, % variance, Fast RGB
    if(ErrorLevel = 2)
    {
        MsgBox, % "Failed to find color '" rgbColor "' at (" coords.x1 "," coords.y1 ") => (" coords.x2 "," coords.y2 ")"
    }
    else if(ErrorLevel = 0)
    {
        coords.btn0x := foundX
        coords.btn0y := foundY
        result := True
    }

return result
}